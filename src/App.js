import logo from './logo.svg';
import './App.css';
import Card from './components/Card';
import SwimLane from './components/SwimLane';
import Modal from './components/Modal';
import NewCardForm from './components/NewCardForm';
import { useState} from 'react';

function App() {

  const [createCardModalVisible, setCreateCardModalVisible] = useState(false);

  const handleCreateCard = () => {
    console.log("Handle create card clicked");
    setCreateCardModalVisible(true);
  };

  const handleModalClose = () => {
    setCreateCardModalVisible(false);
  }

  return (
    <div className="App">
      <header className="todo-header">TODO App</header>
      <div className="swimlane-container">
        <SwimLane title="TODO" cards={[{title: "First TODO", description: "Create my todo app"}]}/>
        <SwimLane title="In Progress" cards={[{title: "I'm DOING!", description: "Making new cards"},{title: "I'm also DOING", description:"Making Cards some more"}]}/>
        <SwimLane title="Done" cards={[{title: "I'm DONE", description: "With my arrays"},{title: "I'm DONE", description:"with the card component"}]}/>
      </div>
      <button onClick={handleCreateCard}>Create new Card</button>
      <Modal visible={createCardModalVisible} onClose={handleModalClose}>asdlfkjalgjalsdkgjalsdkjfl</Modal>
    </div>
  );
}

export default App;
