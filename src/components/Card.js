
function Card(props) {
    const { title, description } = props;
    return (
    <div className="card-component">
        <div className="card-title">{title}</div>
        <div className="card-description">{description}</div>
    </div>
    );
}

export default Card;