import Card from './Card';

function SwimLane(props) {
    const { title, cards } = props;
    return (
        <div className="swimlane-component">
            <div className="swimlane-title">{title}</div>
            {!!cards && cards.map(card => <Card title={card.title} description={card.description}/>)}
        </div>
    );
}

export default SwimLane;