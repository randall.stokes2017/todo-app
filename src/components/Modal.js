
function Modal(props) {
    const {visible, onClose} = props;

    return (
        <div className="modal" style={{display: visible ? "block" : "none"}}>
            <div className="modal-content">
                <div style={{textAlign: "right"}}><i onClick={onClose} className="ico-times" role="img" aria-label="Cancel"></i></div>
                <div>{props.children}</div>
            </div>
            
        </div>
    );
}

export default Modal;